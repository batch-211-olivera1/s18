// console.log("Hello Chong")

// Functions

	// Parameters and Arguments
	

	function printInput() {
		let nickname = prompt ("Enter your nickname: ");
		console.log("Hi, " + nickname);
	}
	// printInput()


	// For other cases, functions can also process data directly passed into it instead of relaying only on global variables and prompt()

	function printName(name){
		console.log("My name is " + name)
	}

	printName("Juana");

	// you can directly pass data into the function
	// the function can then call/use that data which is referred as "name"  within the function
	// "name" is called a parameter
	// a "parameter" acts as a named variable/container that exists only inside of a function
	// it is userd to store information that is provided to a function when it is called or invoked

	// "Juana", the information/data provided directly into a function is called an argument

	// in the following examples, "John" and "Jane" are both arguments since both of them are supplied as information that will be used to print out the full message

	printName("John");
	printName("Jane");

	// variables can also be passed as an argument
	let sampleVariable = "Yui";
	printName(sampleVariable);

	// function arguments cannot be used by a function if there are no parameters provided within the function.

	function checkDivisibilityBy8(num) {
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("is" + num + " divisible by 8");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);


	function checkDivisibilityBy4(num) {
		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("is " + num + " divisible by 4");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(56);
	checkDivisibilityBy8(95);

	// // you can also do the same using prompt(), however, take note that prompt()
	// outputs a string. Strings are not ideal for mathematical computations

		// Functions as Arguments
		// Function parameters can also accept other functions as arguments
		// some complex functions use other functions as arguments to perform more complicated results
		// this will be further seen when we discuss array method

	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction){
		argumentFunction;
	};

	// Adding and removing the parrenthesis "()" impacts the output of JS heavily
	// When a function is used with parenthesis "()", it denotes invoking/calling a function

	invokeFunction(argumentFunction);
	// function used without a parenthesis is normally associated with using the function as an argument to another function

	console.log(argumentFunction);
	// finding more onformation about a function in the console using console.log()

	// Using multiple parameters
	// multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding orders

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);

	}
	createFullName("juan", "Cruz");
	createFullName("Jane", "Dela", "Cruz", "Hello");

	// we can use variable as arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	function printFullName(middleName, firstName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}
	printFullName("Juan","Dela","Cruz");
	// result to "Dela Juan Cruz" because "Juan" =======

	// in JS, providing more/less arguments than the expected parameters will not return an error

	function printFriends(friend, friender, friendest){
		console.log("My three friends are: " + friend + ", " + friender + ", " + friendest);
	}
	printFriends("Estrada", "Marcos", "Duterte");

	// Return Statement
		// "return" statement allows us to output a value from a function to be passed to the line/block of code that invoke/called the function

		function returnFullName(firstName, middleName, lastName){
			// console.log(firstName + " " + middleName + " " + lastName);
			return firstName + " " + middleName + " " + lastName;
			console.log("This message will not be printed.")
			// notice that console.log after the return is no longer printed in the console
			// that is because ideally any line/block of code that comes after the return statment is ignored because it ends the function execution

		}



		// the "return" statement allows us to output a value of a function to be passed to a line/block of code that called the function
		// the "returnFullName" function was called in the same line as declaring a variable

		// whatever value is returned from "returnFullName" function is stored in the "completeName" variable

		let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
		console.log(completeName);

		let completeName2 = returnFullName("Nehemiah", "C.", "Ellorico");
		console.log(completeName2);

		let combination = completeName + completeName2;
		console.log(combination);

		// this way, a function is able to return a value we can further use or manipulate in our program instead of only print/displaying it in the console

		console.log(returnFullName(firstName, middleName, lastName));
		// in this example, console.log() will print the returned value of the returnFullName() function

		function returnAddress(city,country) {
			let fullAddress = city + ", " + country;
			return fullAddress;
		}
		let myAddress = returnAddress("Cebu", "Philippines");
		console.log(myAddress);

		function printPlayerInfo(username,level,job){
			console.log("username:" + username);
			console.log("Level: " + level);
			console.log("Job: " + job);
			return("username: " + username + " Level: " + level + " Job: " + job);
		};

		let user1 = printPlayerInfo("Knight", 95, "Paladin");
		console.log(user1);

		function printMultiply(num1,num2) {
			return num1*num2;
		}

		let product = printMultiply(2,3);
		console.log(product);